<?php
  header('Access-Control-Allow-Origin: *');
  include('cacovid-config.php');


  $lsymptoms = $_POST['symptoms'];
  $lcondition = $_POST['condition'];
  $ltravelled = $_POST['travelled'];
  $lanywhere_widespread = $_POST['anywhere_widespread'];
  $lclose_contact = $_POST['close_contact'];
  $lnear_facility = $_POST['near_facility'];
  
  $stmt=$db_conn->prepare('INSERT INTO corona(symptoms, health_condition, travelled, anywhere_widespread, close_contact, near_facility) VALUES (:first, :second, :third, :fourth, :fifth, :sixth)');
  $stmt->bindParam(':first', $lsymptoms);
  $stmt->bindParam(':second', $lcondition);
  $stmt->bindParam(':third', $ltravelled);
  $stmt->bindParam(':fourth', $lanywhere_widespread);
  $stmt->bindParam(':fifth', $lclose_contact);
  $stmt->bindParam(':sixth', $lnear_facility);
  

  if($stmt->execute()){	
    
    $output = json_encode(array('type'=>'success', 'text' => 'Thank you! Your application has been submitted.'));
  
  }else {
  
    $output = json_encode(array('type'=>'error', 'text' => 'Application could not be subbmitted! Please try again later.'));
  }
  
?>