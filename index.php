<!DOCTYPE html>
<html lang="en" class="full-screen">

<head>
 <meta charset="UTF-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <meta http-equiv="X-UA-Compatible" content="ie=edge" />
 <title>COVID - 19</title>
 <meta name="description" content="A tool to help you understand what to do next about the early detection COVID-19">


 <!--Favicon-->
 <!-- <link rel="shortcut icon" href="./img/favicon.ico" type="image/x-icon" /> -->
 <!-- Font Awesome-->
 <link href="./font/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" />
 <!-- Material Design Bootstrap -->
 <link type="text/css" rel="stylesheet" href="./css/mdb.min.css" media="screen,projection" />
 <!-- Bootstrap core css-->
 <link type="text/css" rel="stylesheet" href="./css/bootstrap.min.css" media="screen,projection" />
 <!-- My Animations css -->
 <link type="text/css" rel="stylesheet" href="./css/animations.css" media="screen,projection" />
 <!-- My custom css -->
 <link type="text/css" rel="stylesheet" href="./css/style.css" media="screen,projection" />
  <style type="text/css">
    #pre_overlay {
      position: fixed;
      z-index: 99999999;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      background-color: #141439;
    }
  </style>

</head>

<body class="home">
  <div id="pre_overlay">
    <img style="top: 50%; left: 50%; transform: translateX(-50%) translateY(-50%); max-width: 500px; position: relative;" src="img/prelaoder.svg" alt="Loading" />
  </div>
 <div class="all-content">

  <section id="landing">
   <header id="landing-header" class="clearfix">
    <!-- logos -->
    <div id="logos" class="d-md-flex justify-content-between align-items-center">
     <div class="navbar-brand pull-left">
      <a id="covid_logo" href="index.php">
       <img src="./img/logo.svg" alt="Covid Logo" />
      </a>
      <a id="NCDC_logo" href="index.php">
       <img src="./img/logoNCDC.svg" alt="NCDC Logo" />
      </a>
     </div>
    </div>
   </header>
   <div id="content" class="d-md-flex flex-row flex-row-reverse justify-content-between">
    <div id="col-img" class="col-12 col-md-6">
     <div class="d-flex flex-column justify-content-center align-content-center">
      <img src="./img/wash_hands.svg" alt="Wash Hand">
     </div>
    </div>
    <div id="col-text" class="col-12 col-md-6">
     <div class="d-flex flex-column justify-content-center align-content-center">
      <h1>COVID - 19 Screening Tool</h1>
      <p>
       This tool can help you understand what to do next about the early detection 
       COVID-19. You will need to answer a few questions about your travel history, 
       symptoms and possible contact with confirmed COVID - 19 cases. Your 
       information will not be shared with anyone.
      </p>
      <a href="#" class="btn btn-red" data-toggle="modal" data-target="#disclaimer">Take the test</a>
     </div>
    </div>
   </div>
  </section>


  <!-- Modal -->
  <div class="modal fade" id="disclaimer" tabindex="-1" role="dialog" aria-labelledby="disclaimerTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h2 class="modal-title" id="disclaimerLongTitle">Disclaimer</h2>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-center">
        <p class="pb-3">
          By using this tool, you agree to its terms and that CA-COVID will not be liable for 
          any harm relating to your use. 
        </p>
        <p class="pb-5">
          Kindly note: Recommendations provided by this tool do not constitute medical 
          advice and should not be used to diagnose or treat medical conditions.
        </p>
        </div>
        <div class="modal-footer">
          <a href="test.php" class="btn btn-red">I agree</a><br>
          <a href="#" class="btn btn-plain" data-dismiss="modal">I don’t agree. Take me back to the homepage</a>
        </div>
      </div>
    </div>
  </div>


 </div>

 <!--jQuery first-->
 <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
 <!-- <script type="text/javascript" src="./js/jquery-2.1.1.min.js"></script> -->
 <!-- jQuery Ui -->
 <script type="text/javascript" src="./js/jquery-ui.min.js"></script>
 <!-- jQuery migrate -->
 <script src="./js/jquery-migrate-1.4.1.min.js"></script>
 <!-- Animations Js -->
 <script type="text/javascript" src="./js/css3-animate-it.js"></script>
 <!-- Bootstrap tooltips -->
 <script type="text/javascript" src="./js/popper.min.js"></script>
 <!-- Bootstrap core JavaScript -->
 <script type="text/javascript" src="./js/bootstrap.min.js"></script>
 <!-- Material Design Bootstrap Javascript -->
 <script type="text/javascript" src="./js/mdb.min.js"></script>
 <!-- custom Javascript -->
 <script type="text/javascript" src="./js/main.js"></script>
 <script type="text/javascript">
  jQuery(window).load(function() {
   jQuery("#pre_overlay").delay(0).animate({
    'opacity': 0
   }, 800, null, function() {
    jQuery('#pre_overlay').fadeOut('slow');
   });
  });
 </script>




</body>

</html>