<!DOCTYPE html>
<html lang="en" class="full-screen">

<head>
 <meta charset="UTF-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <meta http-equiv="X-UA-Compatible" content="ie=edge" />
 <title>COVID - 19 | Take A Test</title>
 <meta name="description" content="A tool to help you understand what to do next about the early detection COVID-19">

 <!--Favicon-->
 <!-- <link rel="shortcut icon" href="./img/favicon.ico" type="image/x-icon" /> -->
 <!-- Font Awesome-->
 <link href="./font/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" />
 <!-- Material Design Bootstrap -->
 <link type="text/css" rel="stylesheet" href="./css/mdb.min.css" media="screen,projection" />
 <!-- Bootstrap core css-->
 <link type="text/css" rel="stylesheet" href="./css/bootstrap.min.css" media="screen,projection" />
 <!-- My Animations css -->
 <link type="text/css" rel="stylesheet" href="./css/animations.css" media="screen,projection" />
 <!-- My custom css -->
 <link type="text/css" rel="stylesheet" href="./css/style.css" media="screen,projection" />

</head>

<body class="test" onload="shows_form_part(1)">
 <div class="all-content">

  <section id="landing">
   <header id="landing-header" class="clearfix">
    <!-- logos -->
    <div id="logos" class="d-md-flex justify-content-between align-items-center">
     <div class="navbar-brand pull-left">
      <a id="covid_logo" href="index.php">
       <img src="./img/logo-white.svg" alt="Covid Logo" />
      </a>
      <a id="NCDC_logo" href="index.php">
       <img src="./img/logoNCDC.svg" alt="NCDC Logo" />
      </a>
     </div>
    </div>
   </header>
   <div id="content" class="d-md-flex flex-row justify-content-between">
    <div id="col-img" class="col-12 col-md-6">
     <div class="d-flex flex-column justify-content-end align-items-center">
      <h3>Don’t forget to wash your hands!!</h3>
      <img src="./img/wash_hands.svg" alt="Wash Hand">
     </div>
    </div>
    <div id="col-text" class="col-12 col-md-6">
     <div class="d-flex flex-column justify-content-center align-content-center">
      <form id="regForm" action="">

       <!-- One "tab" for each step in the form: -->
       <div id="form_part1" class="tab">
        <h2>Are you experiencing any of these symptoms?</h2>
        <span class="instruction">Select all that apply to you</span>
        <p>
         <input id="fever" class="symptoms" name="symptoms[]" type="checkbox" value="Fever, chills or sweating" required>
         <label for="fever"><span>Fever, chills or sweating</span></label>
        </p>
        <p>
         <input id="throat" class="symptoms" name="symptoms[]" type="checkbox" value="Sore throat" required>
         <label for="throat"><span>Sore throat</span></label>
        </p>
        <p>
         <input id="difficulty" class="symptoms" name="symptoms[]" type="checkbox" value="Difficulty breathing (not severe)" required>
         <label for="difficulty"><span>Difficulty breathing (not severe)</span></label>
        </p>
        <p>
         <input id="cough" class="symptoms" name="symptoms[]" type="checkbox" value="Dry cough" required>
         <label for="cough"><span>Dry cough</span></label>
        </p>
        <p>
         <input id="aches" class="symptoms" name="symptoms[]" type="checkbox" value="Body aches" required>
         <label for="aches"><span>Body aches</span></label>
        </p>
        <p>
         <input id="diarrhea" class="symptoms" name="symptoms[]" type="checkbox" value="Vomitting or diarrhea" required>
         <label for="diarrhea"><span>Vomitting or diarrhea</span></label>
        </p>
        <p>
         <input id="tiredness" class="symptoms" name="symptoms[]" type="checkbox" value="Tiredness" required>
         <label for="tiredness"><span>Tiredness</span></label>
        </p>
        <p>
         <input id="none_symptoms" class="symptoms" name="symptoms[]" type="checkbox" value="None of the above" required>
         <label for="none_symptoms"><span>None of the above</span></label>
        </p>

        <div class="form-nav-container">
          <a href="index.php" type="button" class="btn btn-plain"><i class="fa fa-arrow-left" aria-hidden="true"></i> Previous</a>
          <button type="button" id="next_1" class="btn btn-red">Next</button>
        </div>
       </div>

       <div id="form_part2" class="tab">
        <h2>Do you have any underlying health condition?</h2>
        <span class="instruction">Select all that apply to you</span>
        <p>
         <input id="illness" class="condition" name="condition" type="checkbox" value="Asthma or lung illness" required>
         <label for="illness"><span>Asthma or lung illness</span></label>
        </p>
        <p>
         <input id="pregnancy" class="condition" name="condition" type="checkbox" value="Pregnancy" required>
         <label for="pregnancy"><span>Pregnancy</span></label>
        </p>
        <p>
         <input id="diabetets" class="condition" name="condition" type="checkbox" value="Diabetets" required>
         <label for="diabetets"><span>Diabetets</span></label>
        </p>
        <p>
         <input id="kidney" class="condition" name="condition" type="checkbox" value="Kidney Failure" required>
         <label for="kidney"><span>Kidney Failure</span></label>
        </p>
        <p>
         <input id="heart" class="condition" name="condition" type="checkbox" value="Heart diseases" required>
         <label for="heart"><span>Heart diseases</span></label>
        </p>
        <p>
         <input id="obesity" class="condition" name="condition" type="checkbox" value="Extreme obesity" required>
         <label for="obesity"><span>Extreme obesity</span></label>
        </p>
        <p>
         <input id="liver" class="condition" name="condition" type="checkbox" value="Liver failure" required>
         <label for="liver"><span>Liver failure</span></label>
        </p>
        <p>
         <input id="none_condition" class="condition" name="condition" type="checkbox" value="None of the above" required>
         <label for="none_condition"><span>None of the above</span></label>
        </p>

        <div class="form-nav-container">
          <button type="button" class="btn btn-plain" onclick="shows_form_part(1)"><i class="fa fa-arrow-left" aria-hidden="true"></i> Previous</button>
          <button type="button" id="next_2" class="btn btn-red">Next</button>
        </div>
       </div>

       <div id="form_part3" class="tab">
        <h2>In the last 14 days have you travelled internationally?</h2>
        <p>
         <input id="travelled_yes" name="travelled" type="radio" value="Yes" required>
         <label for="travelled_yes"><span>Yes</span></label>
        </p>
        <p>
         <input id="travelled_no" name="travelled" type="radio" value="No" required>
         <label for="travelled_no"><span>No</span></label>
        </p>

        <div class="form-nav-container">
          <button type="button" class="btn btn-plain" onclick="shows_form_part(2)"><i class="fa fa-arrow-left" aria-hidden="true"></i> Previous</button>
          <button type="button" id="next_3" class="btn btn-red">Next</button>
        </div>
       </div>

       <div id="form_part4" class="tab">
        <h2>Have you been anywhere COVID - 19 is widespread?</h2>
        <span class="instruction">Select all that apply to you</span>
        <p>
         <input id="anywhere_widespread_yes" name="anywhere_widespread" type="radio" value="Yes" required>
         <label for="anywhere_widespread_yes"><span>Yes</span></label>
        </p>
        <p>
         <input id="anywhere_widespread_no" name="anywhere_widespread" type="radio" value="No" required>
         <label for="anywhere_widespread_no"><span>No</span></label>
        </p>

        <div class="form-nav-container">
          <button type="button" class="btn btn-plain" onclick="shows_form_part(3)"><i class="fa fa-arrow-left" aria-hidden="true"></i> Previous</button>
          <button type="button" id="next_4" class="btn btn-red">Next</button>
        </div>
       </div>

       <div id="form_part5" class="tab">
        <h2>Have you had any close contact with any COVID - 19 patients?</h2>
        <p>
         <input id="close_contact_yes" name="close_contact" type="radio" value="Yes" required>
         <label for="close_contact_yes"><span>Yes</span></label>
        </p>
        <p>
         <input id="close_contact_no" name="close_contact" type="radio" value="No" required>
         <label for="close_contact_no"><span>No</span></label>
        </p>

        <div class="form-nav-container">
          <button type="button" class="btn btn-plain" onclick="shows_form_part(4)"><i class="fa fa-arrow-left" aria-hidden="true"></i> Previous</button>
          <button type="button" id="next_5" class="btn btn-red">Next</button>
        </div>
       </div>

       <div id="form_part6" class="tab">
        <h2>Do you live or work near a care facility e.g hospitals, isolation centers?</h2>
        <p>
         <input id="near_facility_yes" name="near_facility" type="radio" value="Yes" required>
         <label for="near_facility_yes"><span>Yes</span></label>
        </p>
        <p>
         <input id="near_facility_no" name="near_facility" type="radio" value="No" required>
         <label for="near_facility_no"><span>No</span></label>
        </p>

        <div class="form-nav-container">
          <button type="button" class="btn btn-plain" onclick="shows_form_part(5)"><i class="fa fa-arrow-left" aria-hidden="true"></i> Previous</button>
          <button type="button" id="next_6" name="btn_insert" class="btn btn-red">Next</button>
        </div>
       </div>

      </form>
     </div>
    </div>
   </div>
  </section>
  
  <!-- Modal -->
  <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h2 class="modal-title" id="alertModalLongTitle">Required</h2>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-center">
          <p id="demo-modal"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

 </div>

 <!--jQuery first-->
 <!-- <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script> -->
 <script type="text/javascript" src="./js/jquery-2.1.1.min.js"></script>
 <!-- jQuery Ui -->
 <script type="text/javascript" src="./js/jquery-ui.min.js"></script>
 <!-- jQuery migrate -->
 <script src="./js/jquery-migrate-1.4.1.min.js"></script>
 <!-- Animations Js -->
 <script type="text/javascript" src="./js/css3-animate-it.js"></script>
 <!-- Bootstrap tooltips -->
 <script type="text/javascript" src="./js/popper.min.js"></script>
 <!-- Bootstrap core JavaScript -->
 <script type="text/javascript" src="./js/bootstrap.min.js"></script>
 <!-- Material Design Bootstrap Javascript -->
 <script type="text/javascript" src="./js/mdb.min.js"></script>
 <!-- custom Javascript -->
 <script type="text/javascript" src="./js/main.js"></script>




</body>

</html>