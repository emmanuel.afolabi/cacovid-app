(function($) {
    $(document).ready(function () {
        $('#next_1').click(function() {
            checked = $("input.symptoms:checked").length;
            
            if(!checked) {
                $('#demo-modal').html('You must check at least one checkbox.');
				$('#alertModal').modal({ show : true });
                // alert("You must check at least one checkbox.");
                return false;
            } else {
                shows_form_part(2);
            }
    
        });
        $('#next_2').click(function() {
          checked = $("input.condition:checked").length;
          
            if(!checked) {
                $('#demo-modal').html('You must check at least one checkbox.');
				$('#alertModal').modal({ show : true });
                // alert("You must check at least one checkbox.");
                return false;
            } else {
                shows_form_part(3);
            }
          
        });
        $('#next_3').click(function() {
          checked = $("input[name=travelled]:checked").length;
          
            if(!checked) {
                $('#demo-modal').html('You must provide an answer.');
				$('#alertModal').modal({ show : true });
                // alert("You must provide an answer.");
                return false;
            } else {
                shows_form_part(4);
            }
          
        });
        $('#next_4').click(function() {
          checked = $("input[name=anywhere_widespread]:checked").length;
          
            if(!checked) {
                $('#demo-modal').html('You must provide an answer.');
				$('#alertModal').modal({ show : true });
                // alert("You must provide an answer.");
                return false;
            } else {
                shows_form_part(5);
            }
          
        });
        $('#next_5').click(function() {
          checked = $("input[name=close_contact]:checked").length;
          
            if(!checked) {
                $('#demo-modal').html('You must provide an answer.');
				$('#alertModal').modal({ show : true });
                // alert("You must provide an answer.");
                return false;
            } else {
                shows_form_part(6);
            }
          
        });
        $('#next_6').click(function() {
          checked = $("input[name=near_facility]:checked").length;
          
            if(!checked) {
                $('#demo-modal').html('You must provide an answer.');
				$('#alertModal').modal({ show : true });
                // alert("You must provide an answer.");
                return false;
            } else {
                submit_form();
            }
          
        });
    });
})(jQuery);


//shows #form_part{n} and hides the other parts
function shows_form_part(n){

    var i = 1, p = $("#form_part"+(i).toString());
    while (p.length != 0){

        if (i == n){
            p.show("slide", {direction: "right" }, "slow");
            // p.show("slow");
        }
        else{
            p.hide("slide", {direction: "right" }, "slow");
            // p.hide("slow");
        }
        i++;
        p = $("#form_part"+(i).toString());
    }

}

//Form Submittion
function submit_form() {

    
    var symptoms = [];
    var condition = [];
    var m_data = new FormData();
    $.each($('.symptoms:checked'), function() {
        symptoms.push($(this).val()); 
    });
    $.each($('.condition:checked'), function() {
        condition.push($(this).val()); 
    });
    m_data.append('symptoms', symptoms);
    m_data.append('condition', condition);
    m_data.append('travelled', $('input[name=travelled]:checked').val());
    m_data.append('anywhere_widespread', $('input[name=anywhere_widespread]:checked').val());
    m_data.append('close_contact', $('input[name=close_contact]:checked').val());
    m_data.append('near_facility', $('input[name=near_facility]:checked').val());
    jQuery.ajax({
        url: "save.php",
        data: m_data,
        processData: false,
        contentType: false,
        type: 'POST',
        dataType: 'json',
        success: function(response) {
            window.location = 'success.php';
            // //load json data from server and output message
            // if (response.type == 'error') { //load json data from server and output message
            //     output = '<p class="error">' + response.text + '</p>';
            //     $('#next_6').prop('disabled', false);
            //     window.location = 'http://localhost:8080/covid/test.php';
            // } else {
            //     output = '<p class="success">' + response.text + '</p>';
            //     $('#next_6').prop('disabled', true);
            //     window.location = 'http://localhost:8080/covid/success.php';
            // }
            // $("#mail-status").html(output).slideDown();
        },
        error: function() {}
    });

}

