<!DOCTYPE html>
<html lang="en" class="full-screen">

<head>
 <meta charset="UTF-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <meta http-equiv="X-UA-Compatible" content="ie=edge" />
 <title>COVID - 19 | Success</title>
 <meta name="description" content="A tool to help you understand what to do next about the early detection COVID-19">

 <!--Favicon-->
 <!-- <link rel="shortcut icon" href="./img/favicon.ico" type="image/x-icon" /> -->
 <!-- Font Awesome-->
 <link href="./font/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" />
 <!-- Material Design Bootstrap -->
 <link type="text/css" rel="stylesheet" href="./css/mdb.min.css" media="screen,projection" />
 <!-- Bootstrap core css-->
 <link type="text/css" rel="stylesheet" href="./css/bootstrap.min.css" media="screen,projection" />
 <!-- My Animations css -->
 <link type="text/css" rel="stylesheet" href="./css/animations.css" media="screen,projection" />
 <!-- My custom css -->
 <link type="text/css" rel="stylesheet" href="./css/style.css" media="screen,projection" />

</head>

<body class="success">
 <div class="all-content">

  <section id="landing">
   <header id="landing-header" class="clearfix">
    <!-- logos -->
    <div id="logos" class="d-md-flex justify-content-between align-items-center">
     <div class="navbar-brand pull-left">
      <a id="covid_logo" href="index.php">
       <img src="./img/logo-white.svg" alt="Covid Logo" />
      </a>
      <a id="NCDC_logo" href="index.php">
       <img src="./img/logoNCDC.svg" alt="NCDC Logo" />
      </a>
     </div>
    </div>
   </header>
   <div id="content" class="d-md-flex flex-row justify-content-start">
    <div id="col-text" class="col-12">
     <div class="d-flex flex-column justify-content-center align-content-center">
     <?php 
      include('cacovid-config.php');
      // $q = $db_conn->query("SELECT * FROM corona");
      $q = $db_conn->query("SELECT * FROM corona ORDER BY id DESC LIMIT 1");
      $result = $q->fetch();
       // var_dump($result);
      // //Print out the result for example purposes.
      // echo 'The ID of the last inserted row was: ' . $result['id'];
      function arraycount($array, $value){
       $counter = 0;
       foreach($array as $thisvalue) /*go through every value in the array*/
       {
        if($thisvalue === $value){ /*if this one value of the array is equal to the value we are checking*/
        $counter++; /*increase the count by 1*/
        }
       }
       return $counter;
       } 
       $advice = arraycount($result, "No");
       // echo $advice;
       if ($advice < 4) { ?>
        
        <h3>You should self-siolate</h3>
        <p>
         Based on your answers, you should stay home and away from others. If you can, have a 
         room and bathroom that’s just for you. This can be hard when you’re not feeling well, 
         but it will protect those around you. 
        </p>
        <p>
         Eat well, drink fluids, and get plenty of rest. 
        </p>
        <p class="pb-5">
         Watch for COVID-19 symptoms such as cough, fever, and difficulty breathing. If your 
         symptoms get worse, contact the NCDC through any of these <a href="#">channels.</a>
        </p>
       <?php } else{ ?>

        <h3>You should practice social distancing</h3>
        <p>
         Based on your answers, you’re not experiencing any symptoms however you Should 
         Practice Social Distancing and
        </p>
        <p class="pb-5">
         Help stop the spread, When outside the home, stay at least six feet away from other 
         people, avoid groups, and only use public transit in necessary
        </p>

       <?php } ?>
      <p>
       Follow us on our social media platforms for more information on staying safe
      </p>
      <ul class="social jumbotron-icon">
       <li class="facebook"><a href="https://web.facebook.com/Cacovidng-105898177734978/" target="_blank"><i class="fa fa-2 fa-facebook" aria-hidden="true"></i></a></li>
       <li class="instagram"><a href="https://www.instagram.com/cacovidng/" target="_blank"><i class="fa fa-2 fa-instagram" aria-hidden="true"></i></a></li>
       <li class="twitter"><a href="https://twitter.com/cacovidng" target="_blank"><i class="fa fa-2 fa-twitter" aria-hidden="true"></i></a></li>
       <!-- <li class="youtube"><a href="#" target="_blank"><i class="fa fa-2 fa-youtube-play" aria-hidden="true"></i></a></li>
       <li class="linkedin"><a href="#" target="_blank"><i class="fa fa-2 fa-linkedin" aria-hidden="true"></i></a></li> -->
      </ul>
     </div>
    </div>
   </div>
  </section>

 </div>

 <!--jQuery first-->
 <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
 <!-- <script type="text/javascript" src="./js/jquery-2.1.1.min.js"></script> -->
 <!-- jQuery Ui -->
 <script type="text/javascript" src="./js/jquery-ui.min.js"></script>
 <!-- jQuery migrate -->
 <script src="./js/jquery-migrate-1.4.1.min.js"></script>
 <!-- Animations Js -->
 <script type="text/javascript" src="./js/css3-animate-it.js"></script>
 <!-- Bootstrap tooltips -->
 <script type="text/javascript" src="./js/popper.min.js"></script>
 <!-- Bootstrap core JavaScript -->
 <script type="text/javascript" src="./js/bootstrap.min.js"></script>
 <!-- Material Design Bootstrap Javascript -->
 <script type="text/javascript" src="./js/mdb.min.js"></script>
 <!-- custom Javascript -->
 <script type="text/javascript" src="./js/main.js"></script>
 <script>
  function sendContact() {
   var valid
   valid = validateContact();
   if (valid) {
    //data to be sent to server
    var m_data = new FormData();
    m_data.append('fullName', $('#firstName').val());
    m_data.append('courseName', $('#courseName').val());
    m_data.append('userEmail', $('#userEmail').val());
    m_data.append('phoneName', $('#phone').val());
    jQuery.ajax({
     url: "https://www.onefoundation.ng/forward_course_mail.php",
     // url: "contact_mail.php",
     data: m_data,
     processData: false,
     contentType: false,
     type: 'POST',
     dataType: 'json',
     beforeSend: function() {
      $('.btnAction').prop('disabled', true);
      $('#mail-status').html('<img src="./img/ajax-loader1.gif"/>');
     },
     success: function(response) {
      //load json data from server and output message
      if (response.type == 'error') { //load json data from server and output message
       output = '<p class="error">' + response.text + '</p>';
       $('.btnAction').prop('disabled', false);
      } else {
       output = '<p class="success">' + response.text + '</p>';
       $('.btnAction').prop('disabled', true);
      }
      $("#mail-status").html(output).slideDown();
     },
     error: function() {}
    });
   }
  }

  function validateContact() {
   var valid = true;
   $(".demoInputBox").css("background-color", "");
   $(".info").html("");

   if (!$("#firstName").val()) {
    $("#firstName-info").html("(required)");
    $("#firstName").css("background-color", "#ffffdf");
    valid = false;
   }
   if (!$("#courseName").val()) {
    $("#courseName-info").html("(required)");
    $("#courseName").css("background-color", "#ffffdf");
    valid = false;
   }
   if (!$("#userEmail").val()) {
    $("#userEmail-info").html("(required)");
    $("#userEmail").css("background-color", "#ffffdf");
    valid = false;
   }
   if (!$("#userEmail").val().match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)) {
    $("#userEmail-info").html("(invalid)");
    $("#userEmail").css("background-color", "#ffffdf");
    valid = false;
   }
   if (!$("#phone").val()) {
    $("#phone-info").html("(required)");
    $("#phone").css("background-color", "#ffffdf");
    valid = false;
   }

   return valid;
  }
 </script>
 <script type="text/javascript">
  jQuery(window).load(function() {
   jQuery("#pre_overlay").delay(0).animate({
    'opacity': 0
   }, 800, null, function() {
    jQuery('#pre_overlay').fadeOut('slow');
   });
  });
 </script>




</body>

</html>